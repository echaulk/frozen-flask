<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Reading 4: More ideas about hypothesis testing</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS_CHTML-full" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header>
<h1 class="title">Reading 4: More ideas about hypothesis testing</h1>
</header>
<h1 id="tailed-and-2-tailed-tests">1-tailed and 2-tailed tests</h1>
<p>We started out with a simple hypothesis test; <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> vs. <span class="math inline">\(H_{a}:\mu\ne \mu_{o}\)</span>. This type of test, where the alternative hypothesis contains an inequality (<span class="math inline">\(\ne\)</span>), is termed a 2-tailed test. Why is easiest seen using a picture. Look at the first plot in your R script:</p>

<figure>
<img src="2t.jpeg" alt="Example TA" id="fig:Reinforcement" style="width:5.5in" /><figcaption>Example TA<span label="fig:Reinforcement"></span></figcaption>
</figure>
<p>We will reject our null hypothesis whenever our test statistic lies in either of our rejection zones. Since we have 2 such rejection zones, one at each end of the probability distribution, we term it a <span><strong><span><em>2-TAILED</em></span></strong></span> test. The probability of the test statistic being in either of the rejection zones, <em>if the null hypothesis <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> is <strong><span>true</span></strong></em>, is <span class="math inline">\(\frac{\alpha}{2}+\frac{\alpha}{2}=\alpha\)</span>. The probability of the test statistic being in the do not reject zone is then <span class="math inline">\(1-\alpha\)</span>. Note that this is a schematic, the exact values of the vertical lines dividing the zones will vary depending upon the <span class="math inline">\(\alpha\)</span> chosen, the sample size used, and the degrees of freedom available. .5cm</p>
<p>If we had some belief that the true value of <span class="math inline">\(\mu\)</span> lies above our hypothesised value. That is, we are trying to show, based on some prior conception, that <span class="math inline">\(\mu &gt; \mu_{o}\)</span>. This implies that we are looking only for evidence lying in 1 particular direction, so we are looking for <span class="math inline">\(\bar{X}\)</span> to be <em>greater</em> than <span class="math inline">\(\mu_{o}\)</span>, or equivalently, we are looking for our test statistic to lie at the right extreme of the probability distribution. Conversely, if we had some belief that the true value of <span class="math inline">\(\mu\)</span> lay below our hypothesised value, we would be looking for <span class="math inline">\(\bar{X}\)</span> to be <em>less</em> than <span class="math inline">\(\mu_{o}\)</span>, or equivalently, looking for our test statistic to lie at the left extreme of the probability distribution. .5cm</p>
<p>Another picture illustrates. The top schematic shows the situation when testing <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> vs. <span class="math inline">\(H_{a}:\mu &gt; \mu_{o}\)</span>. Note that the entire rejection zone lies at the right end of the probability distribution. The probability of the test statistic being in the right tail rejection zone, <em>if the null hypothesis <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> is <strong><span>true</span></strong></em>, is <span class="math inline">\(\alpha\)</span> and the probability of not rejecting is <span class="math inline">\(1-\alpha\)</span>. The bottom schematic shows the situation when testing <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> vs. <span class="math inline">\(H_{a}:\mu &lt; \mu_{o}\)</span>. The entire rejection zone lies at the left end of the probability distribution and the probability of the test statistic being in the left tail rejection zone, <em>if the null hypothesis <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> is <strong><span>true</span></strong></em>, is <span class="math inline">\(\alpha\)</span>. Look to the second and third plots in your R script:</p>

<p><img src="RT.jpeg" title="fig:" alt="Example TA" id="fig:Reinforcement" style="width:5.5in" /> <img src="LT.jpeg" title="fig:" alt="Example TA" id="fig:Reinforcement" style="width:5.5in" /></p>
<p>There are a couple of things to take from this. Firstly, the 2-tailed test is inherently more conservative. It is harder to reject the null hypothesis against the 2-tailed alternative, or stated another way, <span class="math inline">\(\bar{X}\)</span> must be further from <span class="math inline">\(\mu_{o}\)</span> to reject when using the <span class="math inline">\(H_{a}:\mu \ne \mu_{o}\)</span>. You can see this from the schematics, the rejection zone for the right tailed test is larger (has probability <span class="math inline">\(\alpha\)</span>) than the right tail rejection zone of the 2 tailed test (has probability <span class="math inline">\(\frac{\alpha}{2}\)</span>), so to get into the rejection zone for the 2-tailed test the test statistic must be larger, and hence <span class="math inline">\(\bar{X}\)</span> must lie further above <span class="math inline">\(\mu_{o}\)</span> to reject the 2-tailed test.</p>
<h1 id="errors">Errors</h1>
<p>The other thing to take from this is that <span class="math inline">\(\alpha\)</span> is the probability of making a mistake! A rejected null hypothesis may in fact be true. If <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> is true, then the distribution shown in the schematic <em>IS</em> the sampling distribution of the test statistic <em>in truth</em>, not just by assumption. If <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> is true, then the probability of getting a test statistic in the rejection zone just by bad luck (sampling variability) is <span class="math inline">\(\alpha\)</span>, meaning the probability of rejecting the null hypothesis, when it is true, is <span class="math inline">\(\alpha\)</span>. Obviously, we want the probability of making an error to be small, so why don’t we always choose very small values for <span class="math inline">\(\alpha\)</span>? The answer is that there is more than one type of error to be made here. .5cm</p>
<p><span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span> is either true, or it is not. Our test will either reject this <span class="math inline">\(H_{o}:\mu=\mu_{o}\)</span>, or it will not. Binary outcomes. Return to the analogy of the court of law we used previously where we had a null hypothesis of innocence, unless and until proof of guilt was seen. Rejecting the null hypothesis here would mean the conviction of an innocent accused. the other error is the failure to convict a guilty accused. It is generally accepted that</p>
<p><span>&quot;It is better that ten guilty persons escape than that one innocent suffer&quot;</span></p>
<p><span>5cm - William Blackstone’s formulation</span></p>
<p>or that it is more important to protect innocence than to punish guilt. Typically, we do the same in hypothesis testing where the burden of proof is on the alternative hypothesis, and the problem set up so that the error represented by falsely rejecting <span class="math inline">\(H_{o}:\)</span> is the worse one of the two possibilities.</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"></th>
<th style="text-align: center;"><span class="math inline">\(H_{o}:\)</span> is true</th>
<th style="text-align: center;"><span class="math inline">\(H_{o}:\)</span> is false</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Test Rejects</td>
<td style="text-align: center;">Type I</td>
<td style="text-align: center;">Desired</td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;">Error</td>
<td style="text-align: center;">Outcome</td>
</tr>
<tr class="odd">
<td style="text-align: center;"></td>
<td style="text-align: center;">Probability <span class="math inline">\(\alpha\)</span></td>
<td style="text-align: center;"></td>
</tr>
<tr class="even">
<td style="text-align: center;">Test does</td>
<td style="text-align: center;">Desired</td>
<td style="text-align: center;">Type II</td>
</tr>
<tr class="odd">
<td style="text-align: center;">not Reject</td>
<td style="text-align: center;">Outcome</td>
<td style="text-align: center;">Error</td>
</tr>
<tr class="even">
<td style="text-align: center;"></td>
<td style="text-align: center;"></td>
<td style="text-align: center;">Probability <span class="math inline">\(\beta\)</span></td>
</tr>
</tbody>
</table>
<p>Even if we feel that the the Type I error, with probability <span class="math inline">\(\alpha\)</span> is the worse mistake to make, we must still control the probability of making the other, Type II, error. That is we must control the probability of failing to reject the null hypothesis when it is in fact false.</p>
<h1 id="power">Power</h1>
<p>Clearly, we want the probability of Type 1 error to be small, so we want <span class="math inline">\(\alpha\)</span> to be small. We also want the probability of Type II error, and hence <span class="math inline">\(\beta\)</span>, to be small. The problem is that, for any given sample, the smaller I make <span class="math inline">\(\alpha\)</span>, the larger I make <span class="math inline">\(\beta\)</span>. Let us suppose that we have a variable of interest, <span class="math inline">\(X\)</span>, and that <span class="math inline">\(X\)</span> is normally distributed with a mean of <span class="math inline">\(\mu=100\)</span> and a standard deviation of <span class="math inline">\(\sigma=15\)</span>. If I were to do a hypothesis test of <span class="math inline">\(H_{o}:\mu=100\)</span> vs. <span class="math inline">\(H_{a}:\mu &gt; 100\)</span> we would have a test statistic of: <span class="math display">\[Z_{obs}=\frac{\bar{X}-100}{\frac{\sigma}{n}},\]</span></p>
<p>and a probability <span class="math inline">\(\alpha\)</span> of incorrectly rejecting the true null hypothesis. But what if I were instead testing <span class="math inline">\(H_{o}:\mu=90\)</span> vs. <span class="math inline">\(H_{a}:\mu &gt; 90\)</span> because that is a likely value based on experience prior to collecting the data?</p>


<p>We can draw the distribution of <span class="math inline">\(\bar{X}\)</span> under the null hypothesis (which <em>assumes</em> <span class="math inline">\(\mu=90\)</span>) easily enough, just keep in mind that it is not the actual distribution from which <span class="math inline">\(\bar{X}\)</span> is being generated. We can draw a normal distribution with <span class="math inline">\(\mu=100\)</span> from which <span class="math inline">\(\bar{X}\)</span> is actually coming. Using an <span class="math inline">\(\alpha=.05\)</span> and a sample size of <span class="math inline">\(n=15\)</span>, we can pretty easily find that we will reject our null hypothesis of <span class="math inline">\(H_{o}:\mu=90\)</span> in favour of <span class="math inline">\(H_{a}:\mu &gt; 90\)</span> if we get <span class="math inline">\(\bar{X}&gt;96.37\)</span>. But <span class="math inline">\(\alpha\)</span> is of no actual consequence here. We know (because we made the problem) that our null hypothesis of <span class="math inline">\(H_{o}:\mu=90\)</span> is false. So we want to reject. So what is the probability of rejecting here? It is the probability of getting <span class="math inline">\(\bar{X}&gt;96.37\)</span>, but measured on the actual distribution, drawn in black on your plot, with <span class="math inline">\(\mu=100\)</span>. We find our <span class="math inline">\(beta\)</span>, our probability of failing to reject the false null, to be 17.4%. Not too good, not too bad. If we use a smaller <span class="math inline">\(\alpha=.01\)</span> we make the problem worse, as our probability of a Type II error rises to <span class="math inline">\(\beta=.3991\)</span>. .5cm</p>
<p>Most statistical researchers work with the concept of <span><strong><span><em>STATISTICAL POWER</em></span></strong></span>, where the power of the test is defined as <span class="math inline">\(1-\beta\)</span>. A <span class="math inline">\(\beta\)</span> close to 0 gives a power close to 1. A <em>powerful</em> statistical test is one that has a high power, and therefore a good ability to detect when the null hypothesis is false. In general, the larger the difference you are looking for, the more power is available for a given sample size. The only way to have high power and small <span class="math inline">\(\alpha\)</span> is to adequate sample sizes. Most statistical software are capable of calculating power as a function of sample size(s).</p>
<p>Modify the code provided to see if you can determine the power (using <span class="math inline">\(\alpha=.05\)</span> and <span class="math inline">\(\alpha=.01\)</span>) for the hypothesis test of <span class="math inline">\(H_{o}:\mu=95\)</span> vs. <span class="math inline">\(H_{a}:\mu &gt; 95\)</span>. You should have less power. Try for <span class="math inline">\(H_{o}:\mu=80\)</span> vs. <span class="math inline">\(H_{a}:\mu &gt; 80\)</span>. You should have much more power. Then try with <span class="math inline">\(n=30\)</span>. Again, you should see more power.</p>
</body>
</html>
