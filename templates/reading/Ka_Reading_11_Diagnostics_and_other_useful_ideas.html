<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <title>Reading 11: Diagnostics, and other useful ideas.</title>
  <style type="text/css">
      code{white-space: pre-wrap;}
      span.smallcaps{font-variant: small-caps;}
      span.underline{text-decoration: underline;}
      div.column{display: inline-block; vertical-align: top; width: 50%;}
  </style>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS_CHTML-full" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<header>
<h1 class="title">Reading 11: Diagnostics, and other useful ideas.</h1>
</header>
<h2 id="diagnostics" class="unnumbered unnumbered">Diagnostics</h2>
<p>So we have our ANOVA model, and we have seen that it provides a flexible and useful way to investigate factors which may influence some response, and gain some insight into the nature of those influences. Like many useful analytical tools, statistical ones especially, ANOVA rests on some assumptions. We want to move from the ANOVA telling us that there is, or is not interaction, or that the levels of the factors are not all the same, to <span><strong><span><em>which</em></span></strong></span> treatments differ, or which <span><strong><span><em>specific</em></span></strong></span> combinations matter, or <span><strong><span><em>how much</em></span></strong></span> the levels of the factor differ. In an earlier reading we touched on assumptions, and did some cursory examination of the data for properties like unimodality and symmetry. While the ANOVA itself is not terribly sensitive to these assumptions, the methods we will need to invoke to answer the more specific questions of <span><strong><span><em>which</em></span></strong></span> and <span><strong><span><em>how much</em></span></strong></span> rely upon them more heavily. So now is the time to re-visit our earlier, very simple, assumption checking and do a little more. We assume that we have data which are:</p>
<ul>
<li><p>Independent observations,</p></li>
<li><p>from normally distributed populations,</p></li>
<li><p>with equal variances.</p></li>
</ul>
<h3 id="independence" class="unnumbered unnumbered">Independence</h3>
<p>The assumption of independence cannot be checked mathematically. Independence is a funtion of the design of the experiment, how the data was collected. We carefully control, or measure, the levels of our factors, we use tools like blocking to control for extraneous factors which can influence our response, and we randomise to &quot;average out&quot;, or randomize over, those other factors (which we may not even know exist). If we fail to account for known nuisance sources of variability, we will have issues of <span><strong><span><em>identifiability</em></span></strong></span>, where we will not be able to say whether the effect is due to the effects of the factor, or the nuisance. We choose <em>random</em> samples, we run trials in <em>random</em> order etc. and avoid situations like doing all the experiments in some order. For example, we don’t want to run experiments like a lab test where we examine at 50% solution, then 60%, then 70%, then 80%. It is better to randomly choose the order of the %ages. What if the spectrometer gets more sensitive as it warms up (but we don’t know that)? Doing it in order means we will have <span><strong><span><em>conflated</em></span></strong></span> the effects of the machine warming with the effect of the factor, without knowing. We randomise to avoid such happenings. If we take such precautions in designing an experiment, then assumptions of independence are reasonable. Entire courses are given on such design of experiments, and I don’t want to write a book here, so the real nugget of information to take away is this; the time to consult the data scientist, or crack the statistics text, is <span><strong><span><em>before</em></span></strong></span> you conduct the experiment, or collect your data. Do not make the all too frequent mistake of collecting some data, then trying to figure out how to analyse it, only to have the statistician tell you that all the data you’ve collected at such great expense doesn’t answer the question of interest, or will require a large investment of expensive, sophisticated analysis to yield some less than ideal results, because the experiment’s design components were not well conceived.</p>
<h3 id="equal-variances" class="unnumbered unnumbered">Equal Variances</h3>
<p>The other assumptions can be checked. The assumption of equal variances is checked by examining the <span class="math inline">\(\epsilon\)</span> terms in our model, <span class="math inline">\({X}_{ijk}=\mu + \tau_{i} + \beta_{j} +\epsilon_{ijk}\)</span>. The assumption that the observations are from populations with equal variances is equivalent to the assumtion that all the residuals in the model, (the <span class="math inline">\(\epsilon\)</span> s) are independent, identically distributed, normal variables, with a mean of zero and a variance of <span class="math inline">\(\sigma^{2}\)</span>, the <span class="math inline">\(\sigma^{2}\)</span> being the same <span class="math inline">\(\sigma^{2}\)</span> which is assumed to be the variance of all the populations of response(s). We write all this compactly as, <span class="math display">\[\epsilon \overset{I.I.D}
{\sim}N(0, \sigma^{2})\]</span></p>
<p>So if we want to check the equality of variances assumption, we need check this condition, <span class="math inline">\(\epsilon \overset{I.I.D}{\sim}N(0, \sigma^{2})\)</span>. This condition can easily be checked by the use of a residuals vs. fits plot. This is exatly what it sounds like, the residuals on the Y axis, the fitted values on the X. You will want to see the vertical spread of the residuals be about the same for every fitted value. If the distribution of the residuals has a notably different vertical spread by fitted value, the assumption is violated. Frequently, to avoid issues of scale, standardized residuals (sr) are used instead where <span class="math inline">\(sr_{i}=\frac{\epsilon_{i}}{SE_{\epsilon}}\)</span>, <span class="math inline">\(SE_{\epsilon}\)</span> being the Standard Error of the Residuals, <span class="math inline">\(SE_{\epsilon}=\sqrt{ \frac{\sum \epsilon^{2}}  {N-1}   }\)</span>.</p>
<h3 id="normality" class="unnumbered unnumbered">Normality</h3>
<p>Previously, we just sort of eyeballed normality by checking for symmetry and unimodality in the data set using a simple histogram. We’re going to do something a little more sophisticated here. The Q-Q plot, or quantile-quantile plot, is a graphical tool used to assess if a set of data plausibly came from some theoretical probability distribution (like a Normal or <span class="math inline">\(\chi^{2}\)</span> or F or whatever). For example, if we assume our data is Normally distributed, we can use a Normal QQ plot to check that assumption. It’s just a visual check, not definitive proof, so it is somewhat subjective, but it allows us to see at-a-glance if our assumption is plausible, and if not, how the assumption is violated and what data points contribute to the violation. The basic idea is this, if I plot any quantity against itself, or against any linear function of itself, I get a straight line. If I had the height of all CFMWC perss. in inches on the X axis, nd the height in cm on the Y axis, I get a straight line. A QQ plot is a scatterplot created by plotting two sets of quantiles against one another. If both sets of quantiles came from the same distribution, we should see the points forming roughly, a straight line. The “quantiles” or “percentiles” are points in your data below which a certain proportion of your data fall. For example, imagine the classic bell-curve standard Normal distribution with a mean of 0. The 0.5 quantile, or 50th percentile, is 0. Half the data lie below 0, half above 0. The 0.10 qantile, or 10th percentile is about -1.28, the 0.95 quantile, or 95th percentile, is about 1.64. 10 percent of the data lie below -1.28, 95 percent of the data lie below 1.64. So the quantiles are just your data sorted in ascending order, with various data points labelled as being the point below which a certain proportion of the data fall. QQ plots take your sample data, sort it in ascending order, and then plot them versus quantiles calculated from a theoretical distribution. The number of quantiles is selected to match the size of your sample data. While Normal QQ Plots are the ones most often used in practice due to so many statistical methods assuming normality, QQ Plots can actually be created for any distribution.</p>
<p>Most often I would do a QQ plot for both the data and the residuals when examining an ANOVA, since both are assumed to be normal. As with the residuals vs. fits above, we are only doing a subjective check, not a definitive analysis, so you are on the lookout for really gross deviations from the expected. Some deviation fom the straight line is to be expected.</p>
<p>The attached R code will take you through these basic diagnostics for the 3-way ANOVA we looked at last reading.</p>
<table>
<thead>
<tr class="header">
<th style="text-align: center;"></th>
<th style="text-align: center;"><span class="math inline">\(P(X_{1})\)</span></th>
<th style="text-align: center;">.20</th>
<th style="text-align: center;">.50</th>
<th style="text-align: center;">.30</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;"><span class="math inline">\(P(X_{2})\)</span></td>
<td style="text-align: center;"></td>
<td style="text-align: center;">25oz</td>
<td style="text-align: center;">40oz</td>
<td style="text-align: center;">65oz</td>
</tr>
<tr class="even">
<td style="text-align: center;">.20</td>
<td style="text-align: center;">25oz</td>
<td style="text-align: center;">.04</td>
<td style="text-align: center;">.10</td>
<td style="text-align: center;">.06</td>
</tr>
<tr class="odd">
<td style="text-align: center;">.50</td>
<td style="text-align: center;">40oz</td>
<td style="text-align: center;">.10</td>
<td style="text-align: center;">.25</td>
<td style="text-align: center;">.15</td>
</tr>
<tr class="even">
<td style="text-align: center;">.30</td>
<td style="text-align: center;">65oz</td>
<td style="text-align: center;">.06</td>
<td style="text-align: center;">.15</td>
<td style="text-align: center;">.09</td>
</tr>
</tbody>
</table>

<p><img src="Hist1.jpeg" alt="image" style="width:6.5in" /></p>
</body>
</html>
